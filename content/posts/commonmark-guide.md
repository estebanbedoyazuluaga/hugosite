---
title: "Guide to CommonMark"
date: 2023-05-10T20:08:13-05:00
---

If you're looking for a simple and easy-to-use syntax for formatting plain text documents, CommonMark is a great choice. This standardized version of Markdown provides a variety of features that make it easy to create well-formatted documents quickly and efficiently.

## Headings

You can create headings in CommonMark by adding one to six `#` symbols before the text. The number of `#` symbols determines the level of the heading. For example:

```md
# Heading 1
## Heading 2
### Heading 3
#### Heading 4
##### Heading 5
###### Heading 6
```

outputs

# Heading 1
## Heading 2
### Heading 3
#### Heading 4
##### Heading 5
###### Heading 6

## Emphasis and Strong Emphasis

You can add emphasis to text by enclosing it in one or two `*` or `_` symbols. One symbol will create italicized text, while two symbols will create bold text. For example:

```md
*This text is italicized.*
_This text is also italicized._

**This text is bold.**
__This text is also bold.__
```

outputs

*This text is italicized.*
_This text is also italicized._

**This text is bold.**
__This text is also bold.__

## Lists

You can create ordered and unordered lists in CommonMark.

### Unordered Lists

Unordered lists can be created by using either `*`, `-`, or `+` symbols. For example:

```md
- Item 1
* Item 2
+ Item 3
```

outputs

- Item 1
* Item 2
+ Item 3

### Ordered Lists

Ordered lists can be created by using numbers followed by a `.`. For example:

```md
1. Item 1
2. Item 2
3. Item 3
```

outputs

1. Item 1
2. Item 2
3. Item 3

## Links and Images

You can create links and images in CommonMark.

### Links

Links can be created by enclosing the link text in square brackets, followed by the URL in parentheses. For example:

```md
[Google](https://www.google.com)
```

outputs

[Google](https://www.google.com)

### Images

Images can be created by using an exclamation mark followed by square brackets containing the alt text, and parentheses containing the image URL. For example:

```md
![alt text](https://www.example.com/image.jpg)
```

outputs

![alt text](https://www.example.com/image.jpg)

## Code Blocks and Inline Code

You can add code blocks and inline code to your CommonMark document.

### Code Blocks

Code blocks can be created by enclosing the code in triple backticks. You can optionally add a language name for syntax highlighting if it's supported by the renderer. For example: 

````
```js
function fizzbuzz(n) {
  for (let i = 1; i <= n; i++) {
    if (i % 3 === 0 && i % 5 === 0) {
      console.log("FizzBuzz");
    } else if (i % 3 === 0) {
      console.log("Fizz");
    } else if (i % 5 === 0) {
      console.log("Buzz");
    } else {
      console.log(i);
    }
  }
}
```
````
outputs 

```js
function fizzbuzz(n) {
  for (let i = 1; i <= n; i++) {
    if (i % 3 === 0 && i % 5 === 0) {
      console.log("FizzBuzz");
    } else if (i % 3 === 0) {
      console.log("Fizz");
    } else if (i % 5 === 0) {
      console.log("Buzz");
    } else {
      console.log(i);
    }
  }
}
```


### Inline Code

Inline code can be created by enclosing the code in a single backtick. For example:

```md
Use the `print()` function to display text in the console.
```

outputs

Use the `print()` function to display text in the console.

## Blockquotes

You can create blockquotes in CommonMark by using the `>` symbol. For example:

```md
> This is a blockquote.
```

outbuts 

> This is a blockquote.

## Horizontal Rules

You can create horizontal rules in CommonMark by using three or more hyphens, underscores, or asterisks on a line by themselves. For example:

```md
---
```

outputs

---

## Tables

You can create tables in CommonMark by using hyphens and vertical bars to separate the columns. For example:

```md
| Name | Age | Gender |
|------|-----|--------|
| John | 25  | Male   |
| Jane | 30  | Female |
```

outputs

| Name | Age | Gender |
|------|-----|--------|
| John | 25  | Male   |
| Jane | 30  | Female |

## Conclusion

CommonMark is a great choice for creating well-formatted documents quickly and efficiently. With its various features for headings, emphasis, lists, links, images, code blocks, blockquotes, horizontal rules, and tables, you can create a variety of document types with ease. Whether you're creating a blog post, technical documentation, or just taking notes, CommonMark is a great tool to have in your toolkit.